#!/usr/bin/env ruby

#Add your package managers here

$debianBased="apt-get"
$archLinux="pacman"
$voidLinux="xbps-install"
$sabayon="equo"
$opensuse="zypper"

#command binding methods for different package managers
def pacman()
  $installCmd = "pacman -S"
  $reinstallCmd = "pacman -S --force"
  $searchCmd = "pacman -Ss"
  $updateCmd = "pacman -Su"
  $syncCmd = "pacman -Sy"
  $syncANDupdateCmd = "pacman -Syu"
  $refreshSyncCmd = "pacman -Syy"
  $removeCmd = "pacman -R"
  $recursiveRemoveCmd = "pacman -Rdd"
  $checkUpdatesCmd = "checkupdates"
  $cleanCmd = "pacman -Rsn $(pacman -Qdtq)"
end

def apt_get()
  $installCmd = "apt-get install"
  $reinstallCmd = "apt-get install --reinstall"
  $searchCmd = "apt-cache search"
  $updateCmd = "apt-get upgrade"
  $syncCmd = "apt-get update"
  $syncANDupdateCmd = "apt-get update; sudo apt-get upgrade"
  $refreshSyncCmd = "apt-get update"
  $removeCmd = "apt-get remove"
  $recursiveRemoveCmd = "apt-get --purge remove"
  $cleanCmd = "apt-get autoremove"
  end

def xbps_install()
  $installCmd = "xbps-install"
  $reinstallCmd = "xbps-install -f"
  $searchCmd = "xbps-query -Rs"
  $updateCmd = "xbps-install -u"
  $syncCmd = "xbps-install -S"
  $syncANDupdateCmd = "xbps-install -Su"
  $refreshSyncCmd = "xbps-install -f -S"
  $removeCmd = "xbps-remove"
  $recursiveRemoveCmd = "xbps-remove -f"
  $cleanCmd = "xbps-remove -O"
end

def equo()
  $installCmd = "equo i"
  $reinstallCmd = "equo i"
  $searchCmd = "equo s"
  $updateCmd = "equo u"
  $syncCmd = "equo up"
  $syncANDupdateCmd = "equo up && equo u"
  $refreshSyncCmd = "equo update --force"
  $removeCmd = "equo rm"
  $recursiveRemoveCmd = "equo rm --norecursive"
  $cleanCmd = "equo cleanup"
end
